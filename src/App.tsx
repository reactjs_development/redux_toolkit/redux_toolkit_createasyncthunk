import React from "react";
import { Provider } from "react-redux";
import ProductList from "./components/productList";
import { store } from "./app/store";
function App() {
  return (
    <Provider store={store}>
      <ProductList />
    </Provider>
  );
}

export default App;